import jsPDF from 'jspdf';
import 'jspdf-autotable';

export class JsPDF {

    downloadPdf(tableId: number, fileName: string, options: object) {
        let doc = new jsPDF();
        if (options.hasOwnProperty("title")) {
            doc.text(options['title'].x, options['title'].y, options['title'].text);
        }
        doc.autoTable(
            {
                headStyles : {fillColor: [255, 255, 255],textColor : [0,0,0]},
                html: tableId,
                willDrawCell: function (data) {
                    var rows = data.table.head;
                    if(data.section === 'head'){
                        if (data.row.index === rows.length - 1) {
                            doc.setFillColor(0, 0, 0);
                            doc.setTextColor(255, 255, 255);
                        }
                    }
                },
                theme: 'grid',

            });
        return doc.save(fileName);
    }
}