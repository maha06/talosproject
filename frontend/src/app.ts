import {PLATFORM} from 'aurelia-pal';
import {RouterConfiguration, Router} from 'aurelia-router';
import {HttpClient} from 'aurelia-fetch-client';
import {inject} from 'aurelia-dependency-injection';

@inject(HttpClient)
export class App {
  router: Router;

    constructor() {
    }

    configureRouter(config: RouterConfiguration, router: Router): void {
        this.router = router;
        config.title = 'Talos AS';
        config.options.pushState = true;
        config.options.root = '/';
        config.mapUnknownRoutes(PLATFORM.moduleName('pages/auth/login'));
        config.map([            
            {
            route: ['login'],
            name: 'login',
            moduleId: PLATFORM.moduleName('pages/auth/login'),
            },           
            {
                route: ['signup'],
                name: 'signup',
                moduleId: PLATFORM.moduleName('pages/auth/signup'),              
            },   
            {
                route: ['admin/dashboard'],
                name: 'admin/dashboard',
                moduleId: PLATFORM.moduleName('pages/admin/dashboard'),              
            },      
            {
                route: ['admin/employee'],
                name: 'employee',
                moduleId: PLATFORM.moduleName('pages/admin/employee/employee'),
                title: 'employee',
            }, 
            {
                route: ['admin/customer'],
                name: 'customer',
                moduleId: PLATFORM.moduleName('pages/admin/customer/customer'),
                title: 'customer',
            },   
            {
                route: ['admin/report/ordenguard'],
                name: 'ordenguard',
                moduleId: PLATFORM.moduleName('pages/admin/reports/ordenguard'),
                title: 'ordenguard',
            },
            {
                route: ['admin/report/mobilescales'],
                name: 'mobilescales',
                moduleId: PLATFORM.moduleName('pages/admin/reports/mobilescales'),
                title: 'mobilescales',
            },
            {
                route: ['admin/report/securitycenter'],
                name: 'securitycenter',
                moduleId: PLATFORM.moduleName('pages/admin/reports/securitycenter'),
                title: 'securitycenter',
            },   
   
        ])
    }
}
