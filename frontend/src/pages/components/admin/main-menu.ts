import {Router} from 'aurelia-router';
import {inject} from 'aurelia-framework';
import {EventAggregator} from 'aurelia-event-aggregator'

@inject(Router, EventAggregator)
export class MainMenu{
    public router;
    public eventAggregator;
    
    constructor(Router, EventAggregator) {
        this.router = Router;
        this.eventAggregator = EventAggregator;
    }
    employee(){        
        this.router.navigate('admin/employee');
    }
    customer(){        
        this.router.navigate('admin/customer');
    }

}