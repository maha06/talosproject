import {inject} from 'aurelia-dependency-injection';
import {Router} from 'aurelia-router';
import {Aurelia} from 'aurelia-framework'
import {ValidationControllerFactory, ValidationRules} from 'aurelia-validation';
import {Api} from "../../shared/api";
import {SharedClass} from "../../shared/shared";


@inject(ValidationControllerFactory, Api, Router, Aurelia)
export class login{
    public router;
    public url;
    public aurelia;
    public password;
    public email;
    public validationController;
    public api;


    constructor(controllerFactory, Api, Router, Aurelia) {
        this.validationController = controllerFactory.createForCurrentScope();
        this.api = Api;     
        this.aurelia = Aurelia;
        this.loginValidation();
        this.router = Router;       
    }

    loginValidation() {
        const SIGNINRULES = ValidationRules
            .ensure("email")
            .required().withMessage("Email is required")
            .matches(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)
            .withMessage('Invalid email format')
            .ensure("password")
            .required().withMessage("Password is required")
            .minLength(8)
            .withMessage('Password must not less than 8 characters')
            .maxLength(1024)
            .withMessage('Password must not be greater than 1024 characters')
            .rules;
        this.validationController.addObject(this, SIGNINRULES);
    }

    login() {               
        this.validationController.validate()

            .then(result => {
                if (result.valid) {
                    
                    var data = {                        
                        email: this.email,
                        password: this.password,
                    };
                    console.log(data);
                    this.api.postData('/api/login', data)
                        .then(response => 
                            console.log(response.text()))
                       // .then(jsonData => {
                         //   console.log(jsonData);
                            // var result = {status: false, message: '', token: '', id: ''};
                            // result = JSON.parse((atob(jsonData)));
                            // if (result.status == true && result.message == 'Email EXISTS') {
                            //     document.cookie = "token=" + result.token;
                            //     document.cookie = "id=" + result.id;
                            //     document.cookie = "workspace=" + this.url;
                            //     this.router.navigate('admin/dashboard');                      
                            // } else if (result.status == false && result.message == 'Email Not Exist!') {
                            //     this.showWorkspaceBind = true;
                            // }else if (result.status == false && result.message == 'Invalid email or password'){
                            //     this.loginBind = true;
                            // }

                     //   });
                }
                else{
                    console.log("issue");
                }
            });
    }
}