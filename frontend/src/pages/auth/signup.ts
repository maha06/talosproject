import {inject} from 'aurelia-dependency-injection';
import {Router} from 'aurelia-router';
import {Aurelia} from 'aurelia-framework';
import {ValidationControllerFactory, ValidationRules} from 'aurelia-validation';
import {Api} from "../../shared/api";
import {SharedClass} from "../../shared/shared";
import environment from "../../environment";


@inject(ValidationControllerFactory, Api, Router, Aurelia)
export class signup{

    public router;
    public url;
    public aurelia;
    public password;
    public confirm_password;
    public email;
    public name;
    public validationController;
    public api;
    public baseUrl;


    constructor(controllerFactory, Api, Router, Aurelia) {
        this.validationController = controllerFactory.createForCurrentScope();
        this.api = Api;     
        this.aurelia = Aurelia;
        this.registerValidation();
        this.router = Router;  
        this.baseUrl = environment.endpoint; 
           
    }

    registerValidation() {
        const SIGNUPRULES = ValidationRules
            .ensure("name")
            .required()
            .ensure("email")
            .required().withMessage("Email is required")
            .matches(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)
            .withMessage('Invalid email format')
            .ensure("password")
            .required().withMessage("Password is required")
            .minLength(8)
            .withMessage('Password must not less than 8 characters')
            .maxLength(1024)
            .withMessage('Password must not be greater than 1024 characters')
            .ensure("confirm_password")
            .required().withMessage("Password Confirmation is required")
            .rules;
        this.validationController.addObject(this, SIGNUPRULES);
    }

    register() {  
               
        this.validationController.validate()

            .then(result => {
                if (result.valid) {
                    
                    var data = {   
                        name: this.name,                     
                        email: this.email,
                        password: this.password,
                        password_confirmation: this.confirm_password 
                    };
                    const axios = require('axios').default; 
                    try{
                        console.log('hi');
                        axios.post(this.baseUrl+'/api/register', data)
                            .then(function(response){
                                console.log('saved successfully')
                        }); 
                    }
                    catch(error)
                    {
                        console.log('bi');
                        console.error(error);
                    }
                    // this.api.postData('/api/register', data)
                    //     .then(response => 
                    //         console.log(response.text()))
                    //    .then(jsonData => {
                    //        console.log(jsonData);
                            // var result = {status: false, message: '', token: '', id: ''};
                            // result = JSON.parse((atob(jsonData)));
                            // if (result.status == true && result.message == 'Email EXISTS') {
                            //     document.cookie = "token=" + result.token;
                            //     document.cookie = "id=" + result.id;
                            //     document.cookie = "workspace=" + this.url;
                            //     this.router.navigate('admin/dashboard');                      
                            // } else if (result.status == false && result.message == 'Email Not Exist!') {
                            //     this.showWorkspaceBind = true;
                            // }else if (result.status == false && result.message == 'Invalid email or password'){
                            //     this.loginBind = true;
                            // }

//                       });
                }
                else{
                    console.log("issue");
                }
            });
    }
    
}