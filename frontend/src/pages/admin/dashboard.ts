import {RouterConfiguration, Router} from 'aurelia-router';
import {Aurelia, inject} from 'aurelia-framework';
import {PLATFORM} from 'aurelia-pal';
import {Api} from "../../shared/api";

@inject(Router, Api)
export class Dashboard{
    public sideNav = true;
    public router;
    public api;

    constructor(Router, api) {
        this.router = Router;        
        this.api = api;
    }


    //configureRouter(config: RouterConfiguration, router: Router): void {
        // this.router = router;
        // config.title = 'Talos AS';             
        // config.map([
            // {
            //     route: ['employee'],
            //     name: 'employee',
            //     moduleId: PLATFORM.moduleName('pages/admin/employee/employee'),
            //     title: 'employee',
            // },                       
      //  ])
   // }
    
    signout() {
        
    }
}