import {Aurelia} from 'aurelia-framework'
import environment from './environment';
import {PLATFORM} from 'aurelia-pal';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'fontawesome';
import 'bootstrap';
import 'assets/css/main.css';
import 'assets/css/color_skins.css';
import 'assets/vendor/font-awesome/css/font-awesome.min.css';
import 'assets/vendor/sweetalert/sweetalert.css';
import 'assets/logo.png';
import 'assets/bundles/libscripts.bundle.js';
import 'axios';
//import 'assets/bundles/vendorscripts.bundle.js';
//import 'assets/bundles/datatablescripts.bundle.js';
import 'assets/vendor/sweetalert/sweetalert.min.js';
//import 'assets/bundles/mainscripts.bundle.js';
//import 'assets/js/pages/tables/jquery-datatable.js';
//import 'assets/js/pages/ui/dialogs.js';
import 'jquery';


export function configure(aurelia: Aurelia) {
  aurelia.use
    .standardConfiguration()
    .plugin(PLATFORM.moduleName('aurelia-validation'))
    .feature(PLATFORM.moduleName('resources/index'))

  aurelia.use.developmentLogging(environment.debug ? 'debug' : 'warn');

  if (environment.testing) {
    aurelia.use.plugin(PLATFORM.moduleName('aurelia-testing'));
  }

  aurelia.start().then(() => aurelia.setRoot(PLATFORM.moduleName('app')));
}
